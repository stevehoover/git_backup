#!/usr/bin/bash

# Usage:
# Run from a git repo backup directory containing only subdirectories of git repositories.
# Every repository will be git pulled.
# This backs up the git repos themselves, but does not ensure that each is reproducibly captured, as it does
# not do any installations of the repos.

for repo in `ls -d */*`
do
  echo "Pulling $repo"
  (cd "$repo" && git pull) || echo "Failed to backup repo: $repo"
done
